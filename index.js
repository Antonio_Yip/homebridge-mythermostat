
'use strict';

var Service, Characteristic;
var request = require("request");

const defaultHeaders = {
	"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.3.5 (KHTML, like Gecko) Version/11.0.1 Safari/604.3.5",
	"Content-Type": "application/json; charset=UTF-8",
	"Accept": "application/json, text/javascript, */*; q=0.01",
	"Connection": "keep-alive"
};

function MythermostatPlatform(log, config){
	this.log      = log;
	this.username = config["username"];
	this.password = config["password"];
	this.UseFahrenheit = null;
	this.CurrentHeatingCoolingStateUUID = (new Characteristic.CurrentHeatingCoolingState()).UUID;
	this.TargetHeatingCoolingStateUUID = (new Characteristic.TargetHeatingCoolingState()).UUID;
	this.CurrentTemperatureUUID = (new Characteristic.CurrentTemperature()).UUID;
	this.TargetTemperatureUUID = (new Characteristic.TargetTemperature()).UUID;
	this.TemperatureDisplayUnitsUUID = (new Characteristic.TemperatureDisplayUnits()).UUID;
	//this.RotationSpeedUUID = (new Characteristic.RotationSpeed()).UUID;
	//this.CurrentHorizontalTiltAngleUUID = (new Characteristic.CurrentHorizontalTiltAngle()).UUID;
	//this.TargetHorizontalTiltAngleUUID = (new Characteristic.TargetHorizontalTiltAngle()).UUID;
	//this.CurrentVerticalTiltAngleUUID = (new Characteristic.CurrentVerticalTiltAngle()).UUID;
	//this.TargetVerticalTiltAngleUUID = (new Characteristic.TargetVerticalTiltAngle()).UUID;

	this.foundAccessories = [];
	this.lastUpdate = null;
	this.sessionID = null;
	this.isFetching = false;
}

module.exports = function(homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;

  homebridge.registerPlatform("homebridge-mythermostat", "Mythermostat", MythermostatPlatform);
}

MythermostatPlatform.prototype = {
  accessories: function(callback) {
		const self = this;

		this.authenticate(function() {
			self.getThermostats(function() {
				callback(self.foundAccessories);
			});
		});
  },
	authenticate: function(callback) {
		this.log("Logging in Mythermostat...");

		const self = this;
		const url = "https://mythermostat.info/api/authenticate/user";
		const form = {
			"Email": this.username,
			"Password": this.password,
			"Application": 0
		};

		this.log(JSON.stringify(form));

		request({
    	url: url,
      headers: defaultHeaders,
    	method: "post",
			timeout: 15000,
			form: form
    }, function(err, response) {
			try {
				if (err) {
					throw(err);

				} else {
					self.log("body: " + response.body);

					const body = JSON.parse(response.body);
					self.sessionID = body.SessionId;

					if (callback) {
						callback();
					}
				}
			} catch (e) {
				self.log(e);

				//retry in 1 minute
				setTimeout( function() {
					self.authenticate(callback);
				}, 60*1000 );
			}
		});
	},
  getThermostats: function(callback) {
		this.log("getting thermostats...");
		this.isFetching = true;

    const url = `https://mythermostat.info/api/thermostats?sessionid=${this.sessionID}`;
  	const self = this;

    request({
    	url: url,
      headers: defaultHeaders,
    	method: "get",
			timeout: 15000
    }, function(err, response) {
			try {
				if (err) {
					throw(err);
				} else {
					self.log("getThermostats response: " + JSON.stringify(response));
					//self.log("body: " + response.body);

					const body = JSON.parse(response.body);

					for (var g = 0; g < body.Groups.length; g++) {
						const group = body.Groups[g];
						const thermostats = group.Thermostats;

						self.updateAccessories(group, thermostats);
						self.lastUpdate = new Date().getTime();
					}
				}
			} catch (e) {
				self.log("There was a problem getting thermostats from: " + url);
				self.log(e)

				//TODO:
				//if e == token expired then authenticate
				self.authenticate();
				// else return cache

			} finally {
				self.lastTry = new Date().getTime();
				self.isFetching = false;

				if (callback) {
					callback();
				}
			}
    });
  },
  updateAccessories: function(group, thermostats) {
		for (var t = 0; t < thermostats.length; t++) {
			const thermostat = thermostats[t];

			var accessory = this.foundAccessories.filter( function(a) {
				return a.id == thermostat.SerialNumber;
			}).shift();

			if (accessory) {
				//accessory.name = thermostat.Room + " Thermostat";
				accessory.airInfo = thermostat;

				// var mode = Characteristic.CurrentHeatingCoolingState.OFF;
				// if (thermostat.RegulationMode == 1) {
				// 	mode = Characteristic.CurrentHeatingCoolingState.AUTO;
				// } else if (thermostat.RegulationMode == 2) {
				// 	mode = Characteristic.CurrentHeatingCoolingState.HEAT;
	  		// }
        //
				// const controlService = this.getServices(accessory)[1];

				// controlService.getCharacteristic(Characteristic.CurrentHeatingCoolingState).setValue(mode);
        //
				// controlService.getCharacteristic(Characteristic.CurrentTemperature).setValue(
				// 	Math.trunc(accessory.Temperature / 100.0)
				// );
        //
				// controlService.getCharacteristic(Characteristic.TargetTemperature).setValue(
				// 	Math.trunc((accessory.RegulationMode == 2 ?
				// 		accessory.ComfortTemperature :
				// 		accessory.SetPointTemp) / 100.0)
				// );
			} else {
				accessory = new MythermostatBridgedAccessory([
					{
						controlService: new Service.Thermostat(thermostat.Room),
						characteristics: [
							Characteristic.CurrentHeatingCoolingState,
							Characteristic.TargetHeatingCoolingState,
							Characteristic.CurrentTemperature,
							Characteristic.TargetTemperature,
							Characteristic.TemperatureDisplayUnits,
						]
					}
				]);
				accessory.platform 			= this;
				accessory.remoteAccessory	= thermostat;
				accessory.id 				= thermostat.SerialNumber;
				accessory.name				= thermostat.Room + " Thermostat";
				accessory.model				= "";
				accessory.manufacturer		= "OJ Electronics";
				accessory.serialNumber		= thermostat.SerialNumber;

				accessory.airInfo = thermostat;

				accessory.buildingId		= group.GroupId;

				this.log("Found thermostat: " + thermostat.Room);
				this.foundAccessories.push(accessory);
			}
		}
  },
  getAccessoryValue: function(callback, characteristic, service, homebridgeAccessory) {
		const self = this;

		if (!this.isFetching) {
			if (self.lastUpdate == null || (self.lastUpdate - new Date().getTime() < -10000) ) {
				self.log(`${homebridgeAccessory.name}: cache expired`);

				if (self.lastTry == null || self.lastTry - new Date().getTime() < -30000 ) {
					self.getThermostats();

				} else {
					//prevent loop
					this.log(`${homebridgeAccessory.name}: cache expired but alreay tried, will try later.`);

					if (self.timeout) {
						clearTimeout(self.timeout);
					}

					self.timeout = setTimeout(function(){
						self.getThermostats();
					}, 30*1000);
				}
			}
		}

		//return cache anyway

		this.log(`${homebridgeAccessory.name}: using cache`);

		const r = homebridgeAccessory.airInfo;

		if (!r) {
			callback();
			return;
		}

		if (characteristic.UUID == homebridgeAccessory.platform.CurrentHeatingCoolingStateUUID) {
			if (r.RegulationMode == 1) {
				callback(undefined, Characteristic.CurrentHeatingCoolingState.AUTO);
			} else if (r.RegulationMode == 2) {
				callback(undefined, Characteristic.CurrentHeatingCoolingState.HEAT);
			} else {
				callback(undefined, Characteristic.CurrentHeatingCoolingState.OFF);
			}
		} else if (characteristic.UUID == homebridgeAccessory.platform.TargetHeatingCoolingStateUUID) {
			if (r.RegulationMode == 1) {
				callback(undefined, Characteristic.TargetHeatingCoolingState.AUTO);
			} else if (r.RegulationMode == 2) {
				callback(undefined, Characteristic.TargetHeatingCoolingState.HEAT);
			} else {
				callback(undefined, Characteristic.TargetHeatingCoolingState.OFF);
			}
		} else if (characteristic.UUID == homebridgeAccessory.platform.CurrentTemperatureUUID) {
			callback(undefined, r.Temperature / 100.0);
		} else if (characteristic.UUID == homebridgeAccessory.platform.TargetTemperatureUUID) {
			var temp = r.SetPointTemp;

			if (r.RegulationMode == 2) {
				temp = r.ComfortTemperature;
			}

			callback(undefined, temp / 100.0);
		// } else if (characteristic.UUID == homebridgeAccessory.platform.TemperatureDisplayUnitsUUID) {
		// 	if (homebridgeAccessory.platform.UseFahrenheit) {
		// 		callback(undefined, Characteristic.TemperatureDisplayUnits.FAHRENHEIT);
		// 	} else {
		// 		callback(undefined, Characteristic.TemperatureDisplayUnits.CELSIUS);
		// 	}
		// 	return;
		// } else if (characteristic.UUID == homebridgeAccessory.platform.RotationSpeedUUID) {
		// 	var SetFanSpeed = r.comp.SetFanSpeed,
		// 		NumberOfFanSpeeds = r.NumberOfFanSpeeds;
		// 	var fanSpeed = SetFanSpeed/NumberOfFanSpeeds*100.0;
		// 	callback(undefined, fanSpeed);
		// 	return;
		// } else if (characteristic.UUID == homebridgeAccessory.platform.CurrentHorizontalTiltAngleUUID ||
		// 		   characteristic.UUID == homebridgeAccessory.platform.TargetHorizontalTiltAngleUUID) {
		// 	var VaneHorizontal = r.VaneHorizontal;
		// 	var HorizontalTilt = -90.0 + 45.0 * (VaneHorizontal - 1);
		// 	callback(undefined, HorizontalTilt);
		// 	return;
		// } else if (characteristic.UUID == homebridgeAccessory.platform.CurrentVerticalTiltAngleUUID ||
		// 		   characteristic.UUID == homebridgeAccessory.platform.TargetVerticalTiltAngleUUID) {
		// 	var VaneVertical = r.VaneVertical;
		// 	var VerticallTilt = 90.0 - 45.0 * (5 - VaneVertical);
		// 	callback(undefined, VerticallTilt);
		// 	return;
		} else {
			callback(undefined, 0);
		}
  },
  setAccessoryValue: function(callback, characteristic, service, homebridgeAccessory, value) {
  	var r = homebridgeAccessory.airInfo;
    var commands = {};
		//{"ComfortTemperature":"2161","RegulationMode":2,"ComfortEndTime":"17/04/2018 05:00:00","VacationEnabled":false}

  	if (characteristic.UUID == homebridgeAccessory.platform.TargetHeatingCoolingStateUUID) {
  		switch (value) {
  			case Characteristic.TargetHeatingCoolingState.OFF:
					commands.RegulationMode = 1;
  				commands.VacationEnabled = true;
  				break;
  			case Characteristic.TargetHeatingCoolingState.HEAT:
					commands.RegulationMode = 2;
					commands.VacationEnabled = false;
					commands.ComfortTemperature = r.ComfortTemperature;
					commands.ComfortEndTime = new Date((new Date().getTime() + 60 * 60 * 1000)).toJSON();
  				break;
  			case Characteristic.TargetHeatingCoolingState.AUTO:
					commands.RegulationMode = 1;
					commands.VacationEnabled = false;
  				break;
  			default:
  				callback();
  				return;
  		}
  	} else
		if (characteristic.UUID == homebridgeAccessory.platform.TargetTemperatureUUID) {
			commands.ComfortTemperature = value * 100.0 ;
  	} else {
  		callback();
  		return;
  	}

    var url = `https://mythermostat.info/api/thermostat?sessionid=${homebridgeAccessory.platform.sessionID}&serialnumber=${homebridgeAccessory.id}`;
  	var body = JSON.stringify(commands);

    request({
      url: url,
      headers: defaultHeaders,
      method: "post",
			timeout: 15000,
      form: body
    }, function(err, response) {
      if (err) {
    		homebridgeAccessory.platform.log("There was a problem sending commands to: " + url);
    		homebridgeAccessory.platform.log(err);
  	  }
      else {
        homebridgeAccessory.platform.log("response: " + JSON.stringify(response));

				try {
					const body = JSON.parse(response.body);

					if (body.Success) {
						Object.keys(commands).forEach(function(key) {
							homebridgeAccessory.airInfo[key] = commands[key];
						});
					}
				} catch (e) {
					homebridgeAccessory.platform.log(e);
				}
      }

  	  callback();
    });
  },
  getInformationService: function(homebridgeAccessory) {
    var informationService = new Service.AccessoryInformation();
    informationService.setCharacteristic(Characteristic.Name, homebridgeAccessory.name)
		.setCharacteristic(Characteristic.Manufacturer, homebridgeAccessory.manufacturer)
		.setCharacteristic(Characteristic.Model, homebridgeAccessory.model)
		.setCharacteristic(Characteristic.SerialNumber, homebridgeAccessory.serialNumber);
  	return informationService;
  },
  bindCharacteristicEvents: function(characteristic, service, homebridgeAccessory) {
		characteristic.on('set', function(value, callback, context) {
			homebridgeAccessory.platform.setAccessoryValue(callback, characteristic, service, homebridgeAccessory, value);
		}.bind(this) );

    characteristic.on('get', function(callback) {
			homebridgeAccessory.platform.getAccessoryValue(callback, characteristic, service, homebridgeAccessory);
		}.bind(this) );
  },
  getServices: function(homebridgeAccessory) {
  	var services = [];
  	var informationService = homebridgeAccessory.platform.getInformationService(homebridgeAccessory);
  	services.push(informationService);
  	for (var s = 0; s < homebridgeAccessory.services.length; s++) {
			var service = homebridgeAccessory.services[s];
			for (var i=0; i < service.characteristics.length; i++) {
				var characteristic = service.controlService.getCharacteristic(service.characteristics[i]);
				if (characteristic == undefined)
					characteristic = service.controlService.addCharacteristic(service.characteristics[i]);
				homebridgeAccessory.platform.bindCharacteristicEvents(characteristic, service, homebridgeAccessory);
			}
			services.push(service.controlService);
    }
    return services;
  }
}

function MythermostatBridgedAccessory(services) {
    this.services = services;
}

MythermostatBridgedAccessory.prototype = {
	getServices: function() {
		var services = [];
		var informationService = this.platform.getInformationService(this);
		services.push(informationService);
		for (var s = 0; s < this.services.length; s++) {
			var service = this.services[s];
			for (var i=0; i < service.characteristics.length; i++) {
				var characteristic = service.controlService.getCharacteristic(service.characteristics[i]);
				if (characteristic == undefined)
					characteristic = service.controlService.addCharacteristic(service.characteristics[i]);
				this.platform.bindCharacteristicEvents(characteristic, service, this);
			}
			services.push(service.controlService);
		}
		return services;
	}
}
